Related to https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/2104

Docs: 

- https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/getting-started.md#the-lighthouse-ci-server
- https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/recipes/heroku-server/README.md

Followed the Heroku recipe. In a seperate branch that I have kept on my local machine for security reasons, I updated the configuration ci file to push to `lchi-server`,  then ran the `yarn lighthouse` script locally to push the results up to that server endpoint.

This app is using a postgresql database to hold the data of each run. It is able to link results of the CI script and tie them to a particular commit on the repository, which is really neat. It is unknown whether we can query against that database, but I cannot see why that wouldn't be possible. If we were ever to use something like this in production, I would recommend creating a backup database to hold the raw data of all of these runs. We would also need to hold environment variables for this the Buyer experience repo to prevent folks from editing, or deleting results on here without necessary credentials. 

One small thing to note is that the `heroku-postgresql:hobby-dev` tier is no longer a thing. I had to put my own credit card info to use their lowest tier, with the ceiling end of hosting this web app being $7/month. 

Update: For the previous month, I was charged $1.40 for compute related to this app. This includes initial setup costs, pushing up 2 test runs, running the server, and then traffic hitting its various endpoints.

To move this from my personal Heroku onto something that isn't owned by me directly, I created a Heroku account using my work email. I had to enroll using 2FA using Okta Verify. 
